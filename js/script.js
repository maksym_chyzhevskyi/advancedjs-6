/*Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.


*/
const ipUrl = "https://api.ipify.org/?format=json"
const physicalUrl = "http://ip-api.com/"
const infoWrapper = document.querySelector('.info-wrapper');
const btn = document.querySelector('.info-button');

btn.addEventListener("click", async function () {
    let ipAdress = await getIp(ipUrl)
    let physicalAdress = await getAdress(physicalUrl, ipAdress)
    renderInfo(physicalAdress)
})

async function getIp(ipUrl) {
    let res = await fetch(ipUrl)
    let data = await res.json()
    return data.ip
}

async function getAdress(physicalUrl, ipAdress) {
    let res = await fetch(`${physicalUrl}json/${ipAdress}?fields=continent,country,region,regionName,city`)
    let location = await res.json()
    return location
}

function renderInfo(physicalAdress) {
    infoWrapper.insertAdjacentHTML('beforeend',
        `<ul>
        <li>Континет - ${physicalAdress.continent}</li>
        <li>Країна - ${physicalAdress.country}</li>
        <li>Регіон - ${physicalAdress.region}</li>
        <li>Місто - ${physicalAdress.city}</li>
        <li>Район - ${physicalAdress.regionName}</li>
      </ul>
        `)
}
